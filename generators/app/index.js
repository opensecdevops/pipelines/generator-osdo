"use strict";
const Generator = require("yeoman-generator");
const chalk = require("chalk");
const yosay = require("yosay");
const os = require("os");

module.exports = class extends Generator {
  async prompting() {
    // Have Yeoman greet the user.
    this.log(
      yosay(`Welcome to the awesome ${chalk.red("generator-osdo")} generator!`)
    );

    const username = os.userInfo().username;

    const { authorName } = await this.prompt({
      type: "input",
      name: "authorName",
      message: "Who is the author of the package?",
      default: username
    });

    const { packageName } = await this.prompt({
      type: "input",
      name: "packageName",
      message: "What is the name of your package?",
      default: this.appname,
      validate: input => {
        const isValid = /^(?:(?:@(?:[a-z0-9-*~][a-z0-9-*._~]*)?\/[a-z0-9-._~])|[a-z0-9-~])[a-z0-9-._~]*$/.test(
          input
        );
        if (!isValid) {
          return 'The "name" field contains your package\'s name, and must be lowercase and one word, and may contain hyphens and underscores.';
        }

        return true;
      }
    });

    const { packageDescription } = await this.prompt({
      type: "input",
      name: "packageDescription",
      message: "What is the description of your package?",
      default: "A fantastic new OSDO package"
    });

    const { packageType } = await this.prompt({
      type: "list",
      name: "packageType",
      message: "What type of package are you going to create?",
      choices: ["Infrastructure", "CI/CD"],
      default: "Infrastructure"
    });

    const { enableUnitTests } = await this.prompt({
      type: "confirm",
      name: "enableUnitTests",
      message: "Would you like to enable unit tests?",
      default: true
    });

    const repoAnswers = await this.prompt([
      {
        type: "input",
        name: "packageRepository",
        message: "What is the URL of your repository?"
      }
    ]);

    this.packageRepository = repoAnswers.packageRepository;

    const homepageAnswers = await this.prompt([
      {
        type: "input",
        name: "packageHomepage",
        message: "What is the URL of the home page?",
        default: this.packageRepository
      }
    ]);

    this.authorName = authorName;
    this.packageHomepage = homepageAnswers.packageHomepage;
    this.packageName = packageName;
    this.packageDescription = packageDescription;
    this.enableUnitTests = enableUnitTests;
    this.packageType = packageType;
  }

  writing() {
    if (this.enableUnitTests) {
      this.fs.copyTpl(
        this.templatePath("package.json"),
        this.destinationPath("package.json"),
        {
          packageName: this.packageName,
          packageDescription: this.packageDescription,
          packageRepository: this.packageRepository,
          packageHomepage: this.packageHomepage,
          authorName: this.authorName
        }
      );

      this.fs.copy(
        this.templatePath("tests/config.test.js"),
        this.destinationPath("tests/config.test.js")
      );

      this.fs.copy(
        this.templatePath("_gitignore"),
        this.destinationPath(".gitignore")
      );
    }

    this.fs.copyTpl(
      this.templatePath("config.json"),
      this.destinationPath("config.json"),
      {
        packageName: this.packageName,
        packageDescription: this.packageDescription,
        packageType: this.packageType,
        packageRepository: this.packageRepository,
        packageHomepage: this.packageHomepage,
        authorName: this.authorName
      }
    );

    this.fs.copy(
      this.templatePath("README.md"),
      this.destinationPath("README.md")
    );

    this.fs.copy(
      this.templatePath("templates/gitlab-ci.hbs"),
      this.destinationPath("templates/gitlab-ci.hbs")
    );
  }

  end() {
    if (this.enableUnitTests) {
      this.spawnCommand("npm", ["install"]).on("close", () => {
        this.log("Las dependencias han sido instaladas.");
      });
    }
  }
};
