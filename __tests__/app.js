"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const helpers = require("yeoman-test");
const os = require("os");

describe("generator-osdo:app", () => {
  it("creates files complete", async () => {
    let spawnCommandCall;
    await helpers
      .run(path.join(__dirname, "../generators/app"))
      .withPrompts({
        packageName: "my-package",
        packageDescription: "little description",
        enableUnitTests: true,
        packageType: "Infrastructure",
        packageRepository: "https://git.com/my-package",
        packageHomepage: "https://opensecdevops.com"
      })
      .on("ready", generator => {
        spawnCommandCall = jest
          .spyOn(generator, "spawnCommand")
          .mockImplementation(() => {
            return {
              on: (event, callback) => event === "close" && callback(0)
            };
          });
      })
      .then(() => {
        assert.file(["package.json"]);
        assert.fileContent("package.json", /"name": "my-package"/);
        assert.fileContent(
          "package.json",
          /"description": "little description"/
        );
        assert.fileContent(
          "package.json",
          /"homepage": "https:\/\/opensecdevops.com"/
        );
        assert.fileContent(
          "package.json",
          /"repository": "https:\/\/git.com\/my-package"/
        );
        assert.file(["config.json"]);
        assert.fileContent("config.json", /"name": "my-package"/);
        assert.fileContent(
          "config.json",
          /"description": "little description"/
        );
        assert.fileContent("config.json", /"type": "Infrastructure"/);
        assert.fileContent(
          "config.json",
          /"homepage": "https:\/\/opensecdevops.com"/
        );
        assert.fileContent(
          "config.json",
          /"repository": "https:\/\/git.com\/my-package"/
        );
        assert.file([
          "tests/config.test.js",
          "templates/gitlab-ci.hbs",
          "README.md",
          ".gitignore"
        ]);

        expect(spawnCommandCall).toHaveBeenCalledWith("npm", ["install"]);

        spawnCommandCall.mockRestore();
      });
  });

  it("creates files with out test", async () => {
    let spawnCommandCall;
    await helpers
      .run(path.join(__dirname, "../generators/app"))
      .withPrompts({
        packageName: "my-package",
        packageDescription: "little description",
        enableUnitTests: false,
        packageType: "Infrastructure",
        packageRepository: "https://git.com/my-package",
        packageHomepage: "https://opensecdevops.com"
      })
      .on("ready", generator => {
        spawnCommandCall = jest
          .spyOn(generator, "spawnCommand")
          .mockImplementation(() => {
            return {
              on: (event, callback) => event === "close" && callback(0)
            };
          });
      })
      .then(() => {
        assert.file(["config.json"]);
        assert.fileContent("config.json", /"name": "my-package"/);
        assert.fileContent(
          "config.json",
          /"description": "little description"/
        );
        assert.fileContent("config.json", /"type": "Infrastructure"/);
        assert.fileContent(
          "config.json",
          /"homepage": "https:\/\/opensecdevops.com"/
        );
        assert.fileContent(
          "config.json",
          /"repository": "https:\/\/git.com\/my-package"/
        );
        assert.file(["templates/gitlab-ci.hbs", "README.md"]);

        assert.noFile([".gitignore", "package.json", "tests/config.test.js"]);

        expect(spawnCommandCall).not.toHaveBeenCalledWith("npm", ["install"]);

        spawnCommandCall.mockRestore();
      });
  });

  it("creates files same repository and homepage", async () => {
    let spawnCommandCall;
    await helpers
      .run(path.join(__dirname, "../generators/app"))
      .withPrompts({
        packageName: "my-package",
        packageDescription: "little description",
        enableUnitTests: false,
        packageType: "Infrastructure",
        packageRepository: "https://git.com/my-package"
      })
      .on("ready", generator => {
        spawnCommandCall = jest
          .spyOn(generator, "spawnCommand")
          .mockImplementation(() => {
            return {
              on: (event, callback) => event === "close" && callback(0)
            };
          });
      })
      .then(() => {
        assert.file(["config.json"]);
        assert.fileContent("config.json", /"name": "my-package"/);
        assert.fileContent(
          "config.json",
          /"description": "little description"/
        );
        assert.fileContent("config.json", /"type": "Infrastructure"/);
        assert.fileContent(
          "config.json",
          /"homepage": "https:\/\/git.com\/my-package"/
        );
        assert.fileContent(
          "config.json",
          /"repository": "https:\/\/git.com\/my-package"/
        );
        assert.file(["templates/gitlab-ci.hbs", "README.md"]);

        assert.noFile([".gitignore", "package.json", "tests/config.test.js"]);

        expect(spawnCommandCall).not.toHaveBeenCalledWith("npm", ["install"]);

        spawnCommandCall.mockRestore();
      });
  });

  it("creates files os system user", async () => {
    let spawnCommandCall;

    await helpers
      .run(path.join(__dirname, "../generators/app"))
      .withPrompts({
        packageName: "my-package",
        packageDescription: "little description",
        enableUnitTests: false,
        packageType: "Infrastructure",
        packageRepository: "https://git.com/my-package"
      })
      .on("ready", generator => {
        spawnCommandCall = jest
          .spyOn(generator, "spawnCommand")
          .mockImplementation(() => {
            return {
              on: (event, callback) => event === "close" && callback(0)
            };
          });
      })
      .then(() => {
        const username = os.userInfo().username;

        assert.file(["config.json"]);
        assert.fileContent("config.json", /"name": "my-package"/);
        assert.fileContent(
          "config.json",
          /"description": "little description"/
        );
        assert.fileContent("config.json", /"type": "Infrastructure"/);
        assert.fileContent(
          "config.json",
          /"homepage": "https:\/\/git.com\/my-package"/
        );
        assert.fileContent(
          "config.json",
          /"repository": "https:\/\/git.com\/my-package"/
        );
        const regex = new RegExp(`"author": "${username}"`);

        assert.fileContent("config.json", regex);
        assert.file(["templates/gitlab-ci.hbs", "README.md"]);

        assert.noFile([".gitignore", "package.json", "tests/config.test.js"]);

        expect(spawnCommandCall).not.toHaveBeenCalledWith("npm", ["install"]);

        spawnCommandCall.mockRestore();
      });
  });
});
